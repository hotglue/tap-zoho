"""
Created on Dec 14, 2019

@author: Juned Jabbar
"""

import copy
import json
import os
import sys
import time

import requests
import singer
import singer.utils as singer_utils
from singer import Transformer, metadata

import tap_zoho.zoho as zoho
from tap_zoho.zoho.exceptions import TapZohoDataNotFoundException, get_error_message

LOGGER = singer.get_logger()


# pylint: disable=unused-argument
def transform_bulk_data_hook(data, typ, schema):
    result = data
    # Zoho can return the value '0.0' for integer typed fields. This
    # causes a schema violation. Convert it to '0' if schema['type'] has
    # integer or number.
    if data == "0.0" and "integer" in schema.get("type", []):
        result = "0"

    # Zoho can return the value 'None' (undefined/empty) for integer typed fields. This
    # causes a schema violation. Convert it to '0' if schema['type'] has
    # integer or number.
    if data is None and (
        "integer" in schema.get("type", []) or "number" in schema.get("type", [])
    ):
        result = "0"

    if data == "" and "null" in schema["type"]:
        result = None

    if data is None and (
        "string" in schema.get("type", []) or "date-time" in schema.get("type", [])
    ):
        result = ""

    return result


def fix_tuple_keys(rec):
    to_return = copy.copy(rec)
    if isinstance(rec, dict):
        for item in rec.items():
            if isinstance(item, tuple):
                key = None
                tuple_value = None
                for value in item:
                    if not isinstance(value, dict):
                        key = value
                    elif key is not None:
                        for _item_key in value.keys():
                            if tuple_value is None:
                                tuple_value = {}
                            tuple_value[_item_key] = value[_item_key]

                    if tuple_value is not None and key is not None:
                        rec[key] = tuple_value
                        to_return = rec
    return to_return


def get_stream_version(catalog_entry, state):
    tap_stream_id = catalog_entry["tap_stream_id"]
    catalog_metadata = metadata.to_map(catalog_entry["metadata"])
    replication_key = catalog_metadata.get((), {}).get("replication-key")

    if singer.get_bookmark(state, tap_stream_id, "version") is None:
        stream_version = int(time.time() * 1000)
    else:
        stream_version = singer.get_bookmark(state, tap_stream_id, "version")

    if replication_key:
        return stream_version
    return int(time.time() * 1000)


def sync_records(zh, catalog_entry, state, counter):
    stream = catalog_entry["stream"]
    schema = catalog_entry["schema"]
    stream_alias = catalog_entry.get("stream_alias")
    catalog_metadata = metadata.to_map(catalog_entry["metadata"])
    replication_key = catalog_metadata.get((), {}).get("replication-key")
    stream_version = get_stream_version(catalog_entry, state)
    activate_version_message = singer.ActivateVersionMessage(
        stream=(stream_alias or stream), version=stream_version
    )

    start_time = singer_utils.now()

    LOGGER.info("Syncing Zoho data for stream {}".format(stream))

    try:
        for rec in zh.get_data(sobject=stream, state=state, catalog=catalog_entry):

            # Rename Id to id to match the schema
            if rec.get("Id"):
                rec["id"] = rec.pop("Id")

            # Reset invalid data of event Participants
            if stream == "events":
                rec = reset_events_data(rec, schema)
            if stream == "roles":
                rec = reset_events_data(rec, schema)
            if stream == "users":
                rec = fix_users_data(rec)

            counter.increment()
            if zh.api_type == "REST":
                with Transformer(pre_hook=transform_bulk_data_hook) as transformer:
                    rec = transformer.transform(rec, schema)
            singer.write_message(
                singer.RecordMessage(
                    stream=(stream_alias or stream),
                    record=rec,
                    version=stream_version,
                    time_extracted=start_time,
                )
            )

            replication_key_value = replication_key and singer_utils.strptime_with_tz(
                rec[replication_key]
            )
            if replication_key_value and replication_key_value <= start_time:
                state = singer.write_bookmark(
                    state,
                    catalog_entry["tap_stream_id"],
                    replication_key,
                    rec[replication_key],
                )
                singer.write_state(state)

                # Tables with no replication_key will send an
                # activate_version message for the next sync
        if not replication_key:
            singer.write_message(activate_version_message)
            state = singer.write_bookmark(
                state, catalog_entry["tap_stream_id"], "version", None
            )

    except TapZohoDataNotFoundException as data_not_found_ex:
        LOGGER.warning(
            "No data found for stream {} issue msg {} ".format(
                stream, get_error_message(data_not_found_ex)
            )
        )

    if not replication_key:
        singer.write_message(activate_version_message)
        state = singer.write_bookmark(
            state, catalog_entry["tap_stream_id"], "version", None
        )


def sync_customview_records(zh, catalog_entry, state, counter, config, catalog_full):
    stream = catalog_entry["stream"]
    schema = catalog_entry["schema"]
    stream_alias = catalog_entry.get("stream_alias")
    catalog_metadata = catalog_entry["metadata"]
    replication_key = None
    stream_version = get_stream_version(catalog_entry, state)
    activate_version_message = singer.ActivateVersionMessage(
        stream=(stream_alias or stream), version=stream_version
    )

    start_time = singer_utils.now()
    module_name = stream.split("CustomViews")[0]

    all_views = get_custom_views(config, module_name)
    for catalog in catalog_metadata:
        if len(catalog["breadcrumb"]) > 0:
            if catalog["breadcrumb"][1] != "id":
                if catalog["metadata"]["selected"] == True:
                    if catalog["breadcrumb"][1] == "data":
                        continue

                    view_detail = get_view_by_name(catalog["breadcrumb"][1], all_views)
                    try:

                        for rec in zh.get_data(
                            sobject=module_name,
                            state=state,
                            catalog=catalog_entry,
                            cvid=view_detail["id"],
                        ):

                            record = {}
                            rec["View_Name"] = catalog["breadcrumb"][1]
                            record = {"id": rec["id"], "data": rec}
                            counter.increment()
                            if zh.api_type == "REST":
                                with Transformer(
                                    pre_hook=transform_bulk_data_hook
                                ) as transformer:
                                    rec = transformer.transform(record, schema)
                            singer.write_message(
                                singer.RecordMessage(
                                    stream=(stream_alias or stream),
                                    record=record,
                                    version=stream_version,
                                    time_extracted=start_time,
                                )
                            )

                            replication_key_value = (
                                replication_key
                                and singer_utils.strptime_with_tz(rec[replication_key])
                            )

                            if (
                                replication_key_value
                                and replication_key_value <= start_time
                            ):
                                state = singer.write_bookmark(
                                    state,
                                    catalog_entry["tap_stream_id"],
                                    replication_key,
                                    rec[replication_key],
                                )
                                singer.write_state(state)

                                # Tables with no replication_key will send an
                                # activate_version message for the next sync
                        if not replication_key:
                            singer.write_message(activate_version_message)
                            state = singer.write_bookmark(
                                state, catalog_entry["tap_stream_id"], "version", None
                            )

                    except TapZohoDataNotFoundException as data_not_found_ex:
                        LOGGER.warning(
                            "No data found for stream {} issue msg {} ".format(
                                stream, get_error_message(data_not_found_ex)
                            )
                        )

                    if not replication_key:
                        singer.write_message(activate_version_message)
                        state = singer.write_bookmark(
                            state, catalog_entry["tap_stream_id"], "version", None
                        )

        LOGGER.info("Syncing Zoho data for stream {}".format(stream))


def get_access_token(CONFIG):
    params = {
        "refresh_token": CONFIG.get("refresh_token"),
        "client_id": CONFIG.get("client_id"),
        "client_secret": CONFIG.get("client_secret"),
        "grant_type": "refresh_token",
    }
    access_token = requests.post(
        url="https://accounts.zoho.com/oauth/v2/token", params=params
    ).json()
    return access_token["access_token"]


def get_custom_views(config, sobject="leads"):
    access_token = get_access_token(config)
    url = f"https://www.zohoapis.com/crm/v2/settings/custom_views?module={sobject}"
    custom_views = requests.get(
        url=url, headers={"Authorization": f"Zoho-oauthtoken {access_token}"}
    ).json()
    return custom_views.get("custom_views", [])


def get_view_by_name(selected_view, all_views):
    for current_view in all_views:
        if selected_view == current_view["display_value"]:
            return current_view
    return None


def create_new_stream(stream_name, schema, fields):
    mdata = metadata.new()
    properties = {}

    # Loop over the object's fields
    for field_name in fields:
        # property_schema, mdata = create_property_schema({'api_name': field_name, 'data_type': schema["properties"][field_name]["type"]},
        #                                                 mdata)
        mdata = metadata.write(mdata, ("properties", field_name), "selected", True)

        # properties[field_name] = property_schema

    mdata = metadata.write(
        mdata, (), "forced-replication-method", {"replication-method": "FULL_TABLE"}
    )

    mdata = metadata.write(mdata, (), "table-key-properties", [])
    mdata = metadata.write(mdata, (), "selected", True)

    entry = {
        "stream": stream_name,
        "tap_stream_id": stream_name,
        "schema": schema,
        "metadata": metadata.to_list(mdata),
    }
    return entry


def reset_events_data(rec, schema):
    try:
        for key in schema["properties"].keys():
            if "type" in schema["properties"][key]:
                if schema["properties"][key]["type"] == "string" and rec[key] == None:
                    rec[key] = ""
                elif schema["properties"][key]["type"] == "object" and rec[key] == None:
                    rec[key] = {}

            if key == "Check_In_By":
                debug = ""

        rec["Participants"] = ""
        rec["Tag"] = ""
        return rec
    except:
        debug = ""


def fix_users_data(rec):
    for key in ["role", "created_by", "Modified_By"]:
        if rec.get(key):
            rec[key] = {"id": rec[key]}
    
    for key in ["Available_for_Assignment", "Available_for_Paid_Assignment"]:
        if rec.get(key):
            rec[key] = bool(rec[key])

    return rec
